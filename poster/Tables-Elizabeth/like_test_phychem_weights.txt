
% Table created by stargazer v.5.2 by Marek Hlavac, Harvard University. E-mail: hlavac at fas.harvard.edu
% Date and time: Wed, Apr 19, 2017 - 05:45:31 PM
\begin{table}[!htbp] \centering 
  \caption{} 
  \label{} 
\begin{tabular}{@{\extracolsep{5pt}} ccccc} 
\\[-1.8ex]\hline 
\hline \\[-1.8ex] 
$Log \mathcal{L}~H_{0}$ & $Log\mathcal{L}~H_{A}$ & LRT test statistic & df & p-value \\ 
\hline \\[-1.8ex] 
-69040 & -63550 & 10980 & 6 &  \textless  2.23e-308  \\ 
\hline \\[-1.8ex] 
\end{tabular} 
\end{table} 
