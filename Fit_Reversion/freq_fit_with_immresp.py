import numpy as np
import csv
from sortinghat import sorting_hat as st
from dbl import dbl as get_rid_repeats
from scipy import stats
import matplotlib.pyplot as plt
import numpy as np
from pylab import*
from scipy import integrate
import pylab
import random
import math
from scipy.optimize import fmin
from scipy.optimize import anneal

from freq_fit_functs_immresp import freqfitscore_iw as fiw

#$ Pick New Patient
#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
ifile=open('GraphReadyPat/GraphReadyPatA.csv')
#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

reader=csv.reader(ifile)
csvimport=[]
for row in reader:
    csvimport.append(row)
ifile.close()


#2. pick position and amino acid to model
#---------------------------------------------   
#Pick Position In Patient
#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
Pos='397.0'
AA='K'
Pat='A'
#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


#1)filter by a position
graphpos=[s for s in csvimport if s[6]==Pos]
#2)filter by an amino acid
pos_aa=[a for a in graphpos if a[0]==AA]
#3)sort by time
our_key=lambda aasample:float(aasample[5])
pos_aa_tmsrt=sorted(pos_aa,key=our_key)
#4)pull off time
data_t=[float(s[5]) for s in pos_aa_tmsrt]
#5)pull off frequency
data_y=[[float(s[4]),float(s[3])] for s in pos_aa_tmsrt]

# get rid of any doubles
#------------------------------------------------------------
rep_rem=get_rid_repeats(data_t,data_y)


# 3. format data for functions
#--------------------------------------------------------------
Patient=Pat
Position=Pos


#set of yes and no experiments in each bracket:[pos,#exp]
y=rep_rem[1]
#y=[[4.0, 9.0],[9.0, 9.0], [3.0, 8.0], [14.0, 16.0]]
#time points of each set of yes and no experiments
x=rep_rem[0]
#x=[169.0, 231.0, 354.0, 448.0]

#creating percentage of positive hits for each set
p=[float(m[0])/float(m[1]) for m in y]
print p

#a=fmin(fiw,(0.00001,0.01,0.01,0.01,0.01,0.01),args=(obsvec,timevec),full_output=1)

#=================================================================
#FIT
#=================================================================


#---------------------------
timevec=np.array(x)
obsvec=np.array(p)
#---------------------------
a=fmin(fiw,(0.00001,0.01,0.01,0.01,0.01,0.01),full_output=1)



