import numpy as np
import csv
from sortinghat import sorting_hat as st
from scipy import stats
import matplotlib.pyplot as plt
import numpy as np
from pylab import*
from scipy import integrate
import pylab
import random
import math
from scipy.optimize import fmin
from scipy.optimize import anneal



#===================================================================
#DATA
#===================================================================

# 1. input data
#-------------------------------------------------------------------

ifile=open('GraphReadyPat/GraphReadyPatNA.csv')
reader=csv.reader(ifile)
csvimport=[]
for row in reader:
    csvimport.append(row)
ifile.close()

#2. pick position and amino acid to model
#---------------------------------------------
poslist,poscat=st(csvimport,7)

Pos='471.0'
AA='T'
Pat='NA'
#1)filter by a position
graphpos=[s for s in csvimport if s[6]==Pos]
#2)filter by an amino acid
pos_aa=[a for a in graphpos if a[0]==AA]
#3)sort by time
our_key=lambda aasample:float(aasample[5])
pos_aa_tmsrt=sorted(pos_aa,key=our_key)
#4)pull off time
data_t=[float(s[5]) for s in pos_aa_tmsrt]
#5)pull off frequency
data_y=[[float(s[4]),float(s[3])] for s in pos_aa_tmsrt]



# 3. format data for functions
#--------------------------------------------------------------
Patient=Pat
Position=Pos


#set of yes and no experiments in each bracket:[pos,#exp]
y=data_y
#time points of each set of yes and no experiments
x=data_t
#creating percentage of positive hits for each set
p=[float(m[0])/float(m[1]) for m in y]
print p
#---------------------------------------------------------------



#=================================================================
#FIT
#=================================================================

#1. Set up data and function being minimized
#------------------------------------------------------------------
trimmed='full' #full #subset1 #subset2
start=0
end=len(x)

days_p_sero=x[start]

# Redefine x, y, p
#---------------------------------------------------------------------
time_trimmed=x[start:end]
start_of_reversion=[time_trimmed[0]]
shiftarray=np.array(start_of_reversion*len(time_trimmed))
rev_time=np.array(time_trimmed)-shiftarray
rev_time=list(rev_time)
x=rev_time
p=p[start:end]
y=y[start:end]

#---------------------------

timevec=np.array(x)
obsvec=np.array(p)
Fo=obsvec[0]
#---------------------------

def freq(t,fo,R):
        #a:make sure exponential isn't too large
        ex=-R*t
        if ex>700.0:
            ex=700.0
        a=math.exp(ex)

        #b: second term
        b=(1.0-fo)
        #dm: write full denominator and make sure not zero
        dm=fo+b*a
        if dm==0:
            return 0.99
        else:
            return fo/dm
        
# 2. write cost function
#----------------------------------------------------------------
def freqfitscore((inputs)):
    R=inputs[0]
    fo=inputs[1]
    def freq(t,fo,R):
        #a:make sure exponential isn't too large
        ex=-R*t
        if ex>700.0:
            ex=700.0
        a=math.exp(ex)

        #b: second term
        b=(1.0-fo)
        #dm: write full denominator and make sure not zero
        dm=fo+b*a
        if dm==0:
            return 0.99
        else:
            return fo/dm

    
    predvec=[freq(t,fo,R) for t in timevec]
    predvec=np.array(predvec)
    diff=(obsvec-predvec)
    diffsq=diff*diff
    return sum(diffsq)

# 3. minimize cost function
#----------------------------------------------------------------
a=fmin(freqfitscore,(0.00001,0.01),full_output=1)
#a=anneal(freqfitscore,(0.02,0.001),full_output=1,maxiter=1000000)

# 4. Create Answer from minimization output
#--------------------------------------------------------------

answ=a[0]
Ransw=float(answ[0])
Foansw=float(answ[1])

tmin=0
tmax=int((x[-1])+50)
timevec2=np.array(range(tmin,tmax,1))
fitvec=np.array([freq(t,Foansw,Ransw) for t in timevec2])


#================================================================
#CREATE CI
#================================================================

# 1. Function to Generate Confidence Interval using Beta Dist
#----------------------------------------------------------------
def CIfunct(X,N,error,a,b):
    ConInt=[0,0]
    X=float(X)
    N=float(N)
    error=float(error)
    border_est=(error/2.0)**(1/N)
    #1.check to see if we have a special border case
    #----------------------------------------------------------
    if X==0:
       lci=0.0
       uci=(1- border_est) 
       ConInt=[lci,uci]

    elif X==N:
        lci= border_est
        uci= 1.0
        ConInt=[lci,uci]
    #-----------------------------------------------------------

    #2. If not proceed with calculating CI from jeffreys interval
    #-----------------------------------------------------------
    else:
        LCI=stats.beta.ppf(error,a,b)
        UCI=stats.beta.ppf(1-error,a,b)

        ConInt=[LCI,UCI]
        
    return ConInt

# 2. Write modified Beta Distribution Function
#------------------------------------------------------------------
def jeffbeta_ppf(alpha,beta,n,X,CI):
    X=float(X)
    N=float(n)
    p=X/n
    #calculate shape parms  
    a=alpha+X
    b=beta+n-X
    #creating Inverse Cumulative Probability Density Function
    x =np.arange(0,100,1,dtype=float)/100
    y =stats.beta.ppf(x,a,b)
    #find CI
    alpha=1.0-CI
    CInt=CIfunct(X,n,alpha,a,b)
    LCI=CInt[0]
    UCI=CInt[1]
    
    return [LCI,p,UCI]

#3. Creating CI vectors for samples
#-----------------------------------------------------------------
CIvec=[]
for s in y:
    CIvec.append(jeffbeta_ppf(0.5,0.5,s[1],s[0],0.90))



lw=array([i[1]-i[0] for i in CIvec])
up=array([i[2]-i[1] for i in CIvec])

#4. create alternate vector to fit to find lower bound of possible slopes
#------------------------------------------------------------------------

'''
oldp=[i for i in p]
newp=[el for el in p]
print newp
#foot of slope
fos=1
#top of slope
tos=2
#sub new sample points in
newp[fos]=newp[fos]+up[fos]
newp[tos]=newp[tos]-lw[tos]
print "               "
print "ALTERNATE VECTORS"
print "will fit lowest estimate for slope between points " +str(fos)+" & " +str(tos)
print "========================================================================="
print "timecourse"
print x
print "old percent"
print oldp
print "new percent"
print newp
print "yes no experiments"
print y
print "==========================================================================="


'''
#================================================================
#PLOTTING
#================================================================
figure()
xmin=x[0]-10
xmax=x[-1]+10
ymin=0
ymax=1.2
v = [xmin, xmax, ymin, ymax]
plt.axis(v)
errorbar(x, p, yerr=[lw,up],linestyle='--',marker='o',markerfacecolor='red')
plt.plot(list(timevec2),list(fitvec),'-')
plt.title("Patient {0} at Position {1}.{2}{3} [dps={6}] \n fo={4} and r={5}".format(Patient,Position,AA,trimmed,Foansw,Ransw,days_p_sero))
plt.xlabel("days post start of reversion")
plt.ylabel("quasispecies frequency")
#=====================================================================================
title="Patient {0} at Position {1}.{2}{3}".format(Patient,Position,AA,trimmed)


#plt.show()


timeframe=[x[start],x[end-1]]
frame="subt1"
fit_type="Good" #(Good,Pass,Poor)



plt.savefig('Freq_Fits_withCI2/'+title+'.png')
fit= "{0},{1},{2},{3},{4},{5},{6}\n".format(Pat,Position,frame,Foansw,Ransw,timeframe,fit_type)



ifile=open('Fit_Freq2.txt','a')
ifile.write(fit)
ifile.close()


"""
ifile=open('Fit_Freq.txt')
reader=csv.reader(ifile)
csvimport=[]
for row in reader:
    print row
    csvimport.append(row)
ifile.close()
"""
