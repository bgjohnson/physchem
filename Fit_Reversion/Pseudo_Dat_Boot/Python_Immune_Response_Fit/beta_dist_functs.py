import numpy as np
from scipy import stats
import math


#================================================================
#CREATE CI
#================================================================

# 1. Function to Generate Confidence Interval using Beta Dist
#----------------------------------------------------------------
def CIfunct(X,N,error,a,b):
    ConInt=[0,0]
    X=float(X)
    N=float(N)
    error=float(error)
    border_est=(error/2.0)**(1/N)
    #1.check to see if we have a special border case
    #----------------------------------------------------------
    if X==0:
       lci=0.0
       uci=(1- border_est) 
       ConInt=[lci,uci]

    elif X==N:
        lci= border_est
        uci= 1.0
        ConInt=[lci,uci]
    #-----------------------------------------------------------

    #2. If not proceed with calculating CI from jeffreys interval
    #-----------------------------------------------------------
    else:
        LCI=stats.beta.ppf(error,a,b)
        UCI=stats.beta.ppf(1-error,a,b)

        ConInt=[LCI,UCI]
        
    return ConInt

# 2. Write modified Beta Distribution Function
#------------------------------------------------------------------
def jeffbeta_ppf(alpha,beta,n,X,CI):
    X=float(X)
    N=float(n)
    p=X/n
    #calculate shape parms  
    a=alpha+X
    b=beta+n-X
    #creating Inverse Cumulative Probability Density Function
    x =np.arange(0,100,1,dtype=float)/100
    y =stats.beta.ppf(x,a,b)
    #find CI
    alpha=1.0-CI
    CInt=CIfunct(X,n,alpha,a,b)
    LCI=CInt[0]
    UCI=CInt[1]
    
    return [LCI,p,UCI]
