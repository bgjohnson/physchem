import random
# This is the function that will give a random variable for a data sample
# with "X" number of positives out of an "n" sample size
#=====================================================================
def random_jefferies_betavariate(X,n):
    alpha=0.5
    beta=0.5
    X=float(X)
    n=float(n)
    a=alpha+X
    b=beta+n-X
    rp=random.betavariate(a,b)
    return rp
