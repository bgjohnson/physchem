
#groups lists by a chosen column
#-->PlotMultipleAA
#-----------------------------
# a list of lists (2D list structure)
# a column (integer)
# |
# |
# V
# a_list: (3D list structure) sorted by chosen column
# cat: (1D list) list of categories in column sorted by

def sorting_hat(a_list,column):
    col=column
    bycolumn=lambda x:x[col]
    from itertools import groupby
    a_list=sorted(a_list,key=bycolumn)
    cat=[]
    gr=[]
    for c,g in groupby(a_list,key=bycolumn):
        cat.append(c)
        gr.append(list(g))
    a_list=gr 
    return a_list,cat
    
a=[1,1,100,1,100,2]
info=[['green',100],
      ['blue',100],
      ['orange',3],
      ['blue',10],
      ['orange',11]]
sortedlist,categories=sorting_hat(info,1)
