import numpy as np
import csv
from scipy import stats
import random
import math
import matplotlib.pyplot as plt
from random_jefferies_betavariate import random_jefferies_betavariate as jbv
from beta_bootstrap_funct2 import boot_beta
from sortinghat import sorting_hat as st
from dbl import dbl as get_rid_repeats
from beta_dist_functs import *



#$ Pick New Patient
#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
ifile=open('/home/elizabeth/Dropbox/CloudFiles/python folder cloud/Fit_Reversion/GraphReadyPat/GraphReadyPatRB.csv')
same_patient='RB'
#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


#1. Import Patient Data
reader=csv.reader(ifile)
csvimport=[]
for row in reader:
    csvimport.append(row)
ifile.close()
poslist,poscat=st(csvimport,7)



#2. Choose Subset of Positions off the List
sub_list=[['449.0','L','sub1',0,3]]

for sub in sub_list:
    Pos=sub[0]
    AA=sub[1]
    Pat=same_patient

    # step 1. pull off time (x) & samples (y) for requested Position and Amino Acid
    #------------------------------------------------------
    #1)filter by a position
    graphpos=[s for s in csvimport if s[6]==Pos]
    #2)filter by an amino acid
    pos_aa=[a for a in graphpos if a[0]==AA]
    #3)sort by time
    our_key=lambda aasample:float(aasample[5])
    pos_aa_tmsrt=sorted(pos_aa,key=our_key)
    #4)pull off time
    data_t=[float(s[5]) for s in pos_aa_tmsrt]
    #5)pull off frequency
    data_y=[[float(s[4]),float(s[3])] for s in pos_aa_tmsrt]
    

    # step 2. get rid of any duplicates (x,y)
    #------------------------------------------------------------
    rep_rem=get_rid_repeats(data_t,data_y)
    

    # step 3. format data for functions (x,y,p)
    #--------------------------------------------------------------

    #set of yes and no experiments in each bracket:[pos,#exp]
    y=rep_rem[1]
    #time points of each set of yes and no experiments
    x=rep_rem[0]
    #creating percentage of positive hits for each set
    p=[float(m[0])/float(m[1]) for m in y]
    print p
    print x
    print y
    
    # step 4. calculate CI intervals for long. sample
    #---------------------------------------------------------------

    CIvec=[]
    for s in y:
        CIvec.append(jeffbeta_ppf(0.5,0.5,s[1],s[0],0.95))
    lw=np.array([i[1]-i[0] for i in CIvec])
    up=np.array([i[2]-i[1] for i in CIvec])

    print lw
    print up

    
    
    # step 5. optional: create pseudo data for long. sample
    #---------------------------------------------------------------
    lw_p=p-lw 
    up_p=p+up 
    pseudo_dat=boot_beta(y,lw_p,p,up_p)
    

    plt.figure()
    plt.plot(list(x),list(p),'-')
    plt.errorbar(x, p, yerr=[lw,up],linestyle='--',marker='o',markerfacecolor='red')
    
    for i in range(0,len(pseudo_dat)):
        plt.plot(list(x),list(np.array(pseudo_dat[i,:])[0]),'o',alpha=0.05)
    
