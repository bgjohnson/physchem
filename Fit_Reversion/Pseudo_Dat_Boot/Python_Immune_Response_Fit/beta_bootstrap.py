import numpy as np
from scipy import stats
import matplotlib.pyplot as plt
import numpy as np
from pylab import*
import random
import math
from random_jefferies_betavariate import random_jefferies_betavariate as jbv

#The set of yes/no experiments [X,n]
#======================================================
x=[1,2,3,4]
y=[[4,10],[3,5],[5,5],[6,7]]
p=[float(q[0])/float(q[1]) for q in y]
p=np.array(p)
lw_CI=np.array([0.2537968524,0.4507197283,0.393037769,0.3123439781])	
up_CI=np.array([0.1170359624,0.4507197283,0.393037769,0.3123439781])

lw_p=p-lw_CI
up_p=p+up_CI
#======================================================================

#Modifying the Data to Feed into the Bootstrap
#=============================================================
fr=np.mat([y,lw_p,p,up_p])
fr=np.transpose(fr)
#=============================================================


vals=[] # a list to put bootstrapped values in

for s in range(0,len(fr)): # pull of one data point at a time
    print fr[s]
    sample=fr[s,0]
    lower_CI=fr[s,1]
    upper_CI=fr[s,3]
    percent=fr[s,2]
    print sample
    dist=[]
    while len(dist)<1000: # try to get 10 samples within interval
        v=jbv(sample[1],sample[1]) #generate random betavariate given data point
        if ( lower_CI < v and v < upper_CI ) : dist.append(v)
    vals.append(dist)

#turn sampled values into time vectors 
boot_vecs=np.mat(vals)  
boot_vecs=np.transpose(boot_vecs) 

#================================================================
#PLOTTING
#================================================================
figure()
errorbar(x, p, yerr=[lw_CI,up_CI],linestyle='--',marker='o',markerfacecolor='red')
plt.plot(x,p,'-')
for i in range(0,len(boot_vecs)):
    plt.plot(x,np.array(boot_vecs[i])[0],'o')


    
#===================================================================================== 
