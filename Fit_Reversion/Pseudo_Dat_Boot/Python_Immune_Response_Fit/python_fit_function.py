
##function gives us frequency of consensus for an array of timepoints
## and some parameters
## time array & parm tuple (v,z,qo,ko) ---> [ function] ---> frequency array
import numpy as np
import math



Parms=(1,2,3,4)
X_dat=np.array([1,2,3,6,6])

def freq_con((parms),x_dat):
    #1 pull off parameters and data from input
    v,z,qo,ko=(1,2,3,4)
    x=x_dat
    #write function to apply to timepoints
    term1=lambda xval,v,ko:ko*np.exp(v*xval)
    term2=lambda xval,z,qo:qo*np.exp(z*xval)
            
    #apply function to x to get ratio
    yterm1=term1(x,v,ko)
    yterm2=term2(x,z,qo)

    qval=yterm1-yterm2

    #use ratio to back calculate frequency 

    fw=qval/(1+qval)

    return(fw)

print freq_con(Parms,X_dat)
