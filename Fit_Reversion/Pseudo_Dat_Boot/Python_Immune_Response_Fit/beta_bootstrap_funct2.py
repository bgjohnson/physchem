from scipy import stats
import numpy as np
import random
import math
from random_jefferies_betavariate import random_jefferies_betavariate as jbv




def boot_beta(y,lw_p,p,up_p):
    
    #1.Modifying the Data too Feed into the Bootstrap
    #=======================================================================
    fr=np.mat([y,lw_p,p,up_p])
    fr=np.transpose(fr)
   

    #2. Loop through each sample and create 1000 pseudo samples for each
    #========================================================================
    vals=[] # list for pseudo-samples

    for s in range(0,len(fr)): # pull off one sample from set at a time
        
        sample=fr[s,0]
        print sample
        lower_CI=fr[s,1]
        upper_CI=fr[s,3]
        percent=fr[s,2]
    
        
        #2.b for each sample within the set create a 1000 pseudo samples
        #=========================================================================
        dist=[]
        ct=0
        boot_num=1000
        while len(dist)<boot_num: # try to get 10 samples within interval
            ct=ct+1
            v=jbv(sample[0],sample[1]) #generate random betavariate given data point
            if ( lower_CI < v and v < upper_CI ) : dist.append(v)
            if(ct>boot_num*4): break # after 1000 ect tries give up
            
        if(len(dist)==boot_num):vals.append(dist)
        else:
            print "error: sample bootstrap timed out at time "+str(s)
            print "data point used to create pseudo vector instead"
            vals.append([percent]*boot_num)
    

    #3. rearrange sampled values into pseudo vectors to return
    #============================================================================
    boot_vecs=np.mat(vals)  
    boot_vecs=np.transpose(boot_vecs)
    return boot_vecs


