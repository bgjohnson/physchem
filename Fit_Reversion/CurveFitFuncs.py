import math
import numpy as np
def SumofSquares(arr_resids):
    sq_resids=(arr_resids)*(arr_resids)
    return np.sum(sq_resids)
        

def Resids(timearr,data_y,stepsize,t_start,t_final,deq1,deq2,deq3):
    step_index=[]
    eq1_index=[]
    eq2_index=[]
    eq3_index=[]
    arrdata_y=np.array(data_y)
    for time in timearr:
       #figure out how many much time post tstart
        time_post=time-t_start
       #figure out how many steps to get there from tstart
        num_steps= math.floor(time_post/stepsize)
       #add this to the step index
        step_index.append(num_steps)
    for index in step_index:
        eq1_index.append(deq1[index])
        eq2_index.append(deq2[index])
        eq3_index.append(deq3[index])
  
    resideq1=arrdata_y-np.array(eq1_index)
    resideq2=arrdata_y-np.array(eq2_index)
    resideq3=arrdata_y-np.array(eq3_index)
    return resideq1,resideq2,resideq3
