import numpy as np
from scipy import integrate

def eq(par,start_t,end_t,step):
     teq = np.arange(start_t, end_t, step)
     k=par[0]
     l=par[1]
     #---------------------------------------
     y0=(1.2,0,0)
     def f(y,t,k,l):
         return (l,k,k+l)

     #---------------------------------------
     ds = integrate.odeint(f, y0, teq, args=(k,l))
     return (ds[:,0],ds[:,1],ds[:,2],teq)

     
def eq2(par,start_t,end_t,step,initial_con):
     teq = np.arange(start_t, end_t, step)
     k=par[0]
     l=par[1]
     i=par[2]
    
     #---------------------------------------
     w=initial_con*1234.0
     m=1234.0-w
     y0=(w,m)#start should be 500-40,000 cp/ml
     def f(y,t,k,l,i):
         return (k*y[0]+0.00001*y[1]-i*y[0],(k-l)*y[1]+0.00001*y[0]-i*y[1])

     #---------------------------------------
     ds = integrate.odeint(f, y0, teq, args=(k,l,i))
     to_eq=ds[:,0]+ds[:,1]
     np.putmask(to_eq, to_eq<0.1, 0.1)
     per_eq=ds[:,0]/to_eq
     return (ds[:,0],ds[:,1],per_eq,teq)

#data_t=[4,20.033,45.00,60.02011,80.34,100.2041]
data_t=[16,55,98,202,271,347]
#data_k=[0,0.5,0.6,0.7,0.8,0.9]
data_k=[0.0,0.0,0.05,0.42,0.85,0.92]
data_y=[0.0,55.5,100.2,80,250,312.]
timestep=0.001
t_start=0
t_end=500
parms=[0.0,0.04,1,1]

deq1,deq2,deq3,dt=eq2(parms,t_start,t_end,timestep,0.8)
