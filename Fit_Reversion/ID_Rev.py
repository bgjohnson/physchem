#This is a file to read in Data for Graphing
import matplotlib.pyplot as plt
import numpy as np
import math
import csv
from sortinghat import sorting_hat as st
ifile=open('GraphReadyPatA.csv')
reader=csv.reader(ifile)
csvimport=[]
for row in reader:
    csvimport.append(row)
ifile.close()

#Pull off specific sets of Data:
#----------------------------------------------
#AA [0]
#%Quasispecies [1]
#CI Lower  CI upper [2]
#Total Samples Taken [3]
#Sample Number  [4]
#Time of Sample [5]
#Posistion in Genome [6]
#Region of Genome[7]
#Consenus Typically at Posistion  [8]
#Reversion Status: Did this eventually revert[9]
#------------------------------------------------
poslist,poscat=st(csvimport,6)

#1)Pull of Reversion Data for a Posistion
Pos='459.0'
pat_pos=[s for s in csvimport if s[6]==Pos]

#2)ID Consensus Value
Con=pat_pos[0][8]

#3)Pull off reversion
pat_pos_rev=[aal for aal in pat_pos if aal[0][0]==Con]

#4)Sort Reversion Data by Time:
sortbytime=lambda x:float(x[5])
pat_pos_rev_sort=sorted(pat_pos_rev,key=sortbytime)

#5)pull of x and y values if reversion
fx=lambda x:float(x[5])
fy=lambda x:float(x[1])
fm=lambda x:1.0-x
rx=[fx(x) for x in pat_pos_rev_sort]
rw=[fy(x) for x in pat_pos_rev_sort]
rm=[fm(x) for x in rw]


