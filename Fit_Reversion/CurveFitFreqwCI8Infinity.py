import numpy as np
import csv
from sortinghat import sorting_hat as st
from scipy import stats
import matplotlib.pyplot as plt
import numpy as np
from pylab import*
from scipy import integrate
import pylab
import random
import math
from scipy.optimize import fmin
from scipy.optimize import anneal



#===================================================================
#DATA
#===================================================================

Pat='OW'
Position=114.0
AA='K'




fit_type='sub3_low_est'
dps=288.0
oldp=array([1,1,0.6])
#y=array([1,2])
x=array([0,94,181])
#new_p=array([1,	1,	1,	1,	1-0.4507,	0.39303	,	0])
new_p=array([1,0.606962231,0.6])
lw=array([0.2209221919,0.393037769,0.2731526027])
up=array([0,0,0.2250236556])
timeframe=[x[0],x[len(x)-1]]



p=new_p



trimmed=' '+'full_low_est' #full #sub1 #sub2
days_p_sero=x[0]
timevec=np.array(x)
obsvec=np.array(p)
Fo=obsvec[0]


def freq(t,fo,R):
        #a:make sure exponential isn't too large
        ex=-R*t
        if ex>700.0:
            ex=700.0
        a=math.exp(ex)

        #b: second term
        b=(1.0-fo)
        #dm: write full denominator and make sure not zero
        dm=fo+b*a
        if dm==0:
            return 0.99
        else:
            return fo/dm
        
# 2. write cost function
#----------------------------------------------------------------
def freqfitscore((inputs)):
    R=inputs[0]
    fo=inputs[1]
    def freq(t,fo,R):
        #a:make sure exponential isn't too large
        ex=-R*t
        if ex>700.0:
            ex=700.0
        a=math.exp(ex)

        #b: second term
        b=(1.0-fo)
        #dm: write full denominator and make sure not zero
        dm=fo+b*a
        if dm==0:
            return 0.99
        else:
            return fo/dm

    
    predvec=[freq(t,fo,R) for t in timevec]
    predvec=np.array(predvec)
    diff=(obsvec-predvec)
    diffsq=diff*diff
    return sum(diffsq)


# 3. minimize cost function
#----------------------------------------------------------------
a=fmin(freqfitscore,(0.00001,0.01),full_output=1)


# 4. Create Answer from minimization output
#--------------------------------------------------------------

answ=a[0]
Ransw=float(answ[0])
Foansw=float(answ[1])

tmin=0
tmax=int((x[-1])+50)
timevec2=np.array(range(tmin,tmax,1))
fitvec=np.array([freq(t,Foansw,Ransw) for t in timevec2])




#4. create alternate vector to fit to find lower bound of possible slopes
#------------------------------------------------------------------------


#================================================================
#PLOTTING
#================================================================
figure()
xmin=x[0]-10
xmax=x[-1]+10
ymin=0
ymax=1.2
v = [xmin, xmax, ymin, ymax]
plt.axis(v)
errorbar(x,oldp,yerr=[lw,up],linestyle='--',marker='o',markerfacecolor='red')
plt.plot(list(timevec),list(p),'o')
plt.plot(list(timevec2),list(fitvec),'-')

days_p_sero=dps
plt.title("Patient {0} at Position {1}.{2}{3} [dps={6}] \n fo={4} and r={5}".format(Pat,Position,AA,trimmed,("%.4g" % Foansw),("%.4g" % Ransw),days_p_sero))
plt.xlabel("days post start of reversion")
plt.ylabel("quasispecies frequency")
#=====================================================================================
title="Patient {0} at Position {1}.{2}{3}".format(Pat,Position,AA,trimmed)




plt.savefig('Freq_Fits/Graph_Freq_OW/'+title+'.png')
fit= "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16}\n".format(Pat,Position,AA,fit_type,Foansw,Ransw,timeframe[0],timeframe[1],fit_type,
    "time",str(x),"freq_con",str(p),"lw_CI", str(list(lw)),"up_CI",str(list(up)))


ifile=open('Freq_Fits_Stored/Fit_Freq_OW.csv','a')
ifile.write(fit)
ifile.close()



