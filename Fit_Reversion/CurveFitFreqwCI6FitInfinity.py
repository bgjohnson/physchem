import numpy as np
import csv
from sortinghat import sorting_hat as st
from scipy import stats
import matplotlib.pyplot as plt
import numpy as np
from pylab import*
from scipy import integrate
import pylab
import random
import math
from scipy.optimize import fmin
from scipy.optimize import anneal



#===================================================================
#DATA
#===================================================================

# 3. format data for functions
#--------------------------------------------------------------
Pos='83.0'
AA='A'
Pat='NA'
Patient=Pat
Position=Pos


#set of yes and no experiments in each bracket:[pos,#exp]
y= [[4.0, 9.0], [9.0, 9.0]]
#time points of each set of yes and no experiments
x=[0.0, 62.0]
#creating percentage of positive hits for each set
p=[0.65036651897245901, 0.71687116443688648]
oldp=[0.4444444444444444, 1.0]
#---------------------------------------------------------------

#=================================================================
#FIT
#=================================================================

#1. Set up data and function being minimized
#------------------------------------------------------------------
trimmed='sub (low est)'
start=0
end=len(x)+start
days_p_sero=x[start]

# Redefine x, y, p
#---------------------------------------------------------------------
time_trimmed=x[start:end]
print time_trimmed
start_of_reversion=[time_trimmed[0]]
print start_of_reversion
shiftarray=np.array(start_of_reversion*len(time_trimmed))
print shiftarray
rev_time=np.array(time_trimmed)-shiftarray
print rev_time
rev_time=list(rev_time)
print rev_time
x=rev_time
print x

p=p[start:end]
print p
y=y[start:end]
print y

#---------------------------

timevec=np.array(x)
obsvec=np.array(p)
Fo=obsvec[0]
#---------------------------

def freq(t,fo,R):
        #a:make sure exponential isn't too large
        ex=-R*t
        if ex>700.0:
            ex=700.0
        a=math.exp(ex)

        #b: second term
        b=(1.0-fo)
        #dm: write full denominator and make sure not zero
        dm=fo+b*a
        if dm==0:
            return 0.99
        else:
            return fo/dm
        
# 2. write cost function
#----------------------------------------------------------------
def freqfitscore((inputs)):
    R=inputs[0]
    fo=inputs[1]
    def freq(t,fo,R):
        #a:make sure exponential isn't too large
        ex=-R*t
        if ex>700.0:
            ex=700.0
        a=math.exp(ex)

        #b: second term
        b=(1.0-fo)
        #dm: write full denominator and make sure not zero
        dm=fo+b*a
        if dm==0:
            return 0.99
        else:
            return fo/dm

    
    predvec=[freq(t,fo,R) for t in timevec]
    predvec=np.array(predvec)
    diff=(obsvec-predvec)
    diffsq=diff*diff
    return sum(diffsq)

# 3. minimize cost function
#----------------------------------------------------------------
a=fmin(freqfitscore,(0.20,0.001),full_output=1)
#a=anneal(freqfitscore,(0.02,0.001),full_output=1,maxiter=1000000)

# 4. Create Answer from minimization output
#--------------------------------------------------------------

answ=a[0]
Ransw=float(answ[0])
Foansw=float(answ[1])

tmin=0
tmax=int((x[-1])+50)
timevec2=np.array(range(tmin,tmax,1))
fitvec=np.array([freq(t,Foansw,Ransw) for t in timevec2])


#================================================================
#CREATE CI
#================================================================

# 1. Function to Generate Confidence Interval using Beta Dist
#----------------------------------------------------------------
def CIfunct(X,N,error,a,b):
    ConInt=[0,0]
    X=float(X)
    N=float(N)
    error=float(error)
    border_est=(error/2.0)**(1/N)
    #1.check to see if we have a special border case
    #----------------------------------------------------------
    if X==0:
       lci=0.0
       uci=(1- border_est) 
       ConInt=[lci,uci]

    elif X==N:
        lci= border_est
        uci= 1.0
        ConInt=[lci,uci]
    #-----------------------------------------------------------

    #2. If not proceed with calculating CI from jeffreys interval
    #-----------------------------------------------------------
    else:
        LCI=stats.beta.ppf(error,a,b)
        UCI=stats.beta.ppf(1-error,a,b)

        ConInt=[LCI,UCI]
        
    return ConInt

# 2. Write modified Beta Distribution Function
#------------------------------------------------------------------
def jeffbeta_ppf(alpha,beta,n,X,CI):
    X=float(X)
    N=float(n)
    p=X/n
    #calculate shape parms  
    a=alpha+X
    b=beta+n-X
    #creating Inverse Cumulative Probability Density Function
    x =np.arange(0,100,1,dtype=float)/100
    y =stats.beta.ppf(x,a,b)
    #find CI
    alpha=1.0-CI
    CInt=CIfunct(X,n,alpha,a,b)
    LCI=CInt[0]
    UCI=CInt[1]
    
    return [LCI,p,UCI]

#3. Creating CI vectors for samples
#-----------------------------------------------------------------
CIvec=[]
for s in y:
    CIvec.append(jeffbeta_ppf(0.5,0.5,s[1],s[0],0.90))



lw=array([i[1]-i[0] for i in CIvec])
up=array([i[2]-i[1] for i in CIvec])


#================================================================
#PLOTTING
#================================================================
figure()
xmin=x[0]-10
xmax=x[-1]+10
ymin=0
ymax=1.2
v = [xmin, xmax, ymin, ymax]
plt.axis(v)
errorbar(x, oldp, yerr=[lw,up],linestyle='',marker='o')
plt.plot(x,oldp,'bo')
plt.plot(x,p,'ro')
plt.plot(list(timevec2),list(fitvec),'--')
plt.title("Patient {0} at Position {1}.{2}{3} [dps={6}] \n fo={4} and r={5}".format(Patient,Position,AA,trimmed,Foansw,Ransw,days_p_sero))
plt.xlabel("days post start of reversion")
plt.ylabel("quasispecies frequency")
#=====================================================================================
title="Patient {0} at Position {1}.{2}{3}".format(Patient,Position,AA,trimmed)
#plt.show()


plt.savefig('Freq_Fits_withCI2/'+title+'.png')
fit= "{0},{1},{2},{3},{4},{5},{6}\n".format(Pat,Position,AA,start,end,Foansw,Ransw)



ifile=open('Fit_Freq.txt','a')
ifile.write(fit)
ifile.close()

'''
ifile=open('Fit_Freq.txt')
reader=csv.reader(ifile)
csvimport=[]
for row in reader:
    print row
    csvimport.append(row)
ifile.close()
'''

