import numpy as np
from scipy import integrate
import pylab
import random
import math
from scipy.optimize import fmin
from scipy.optimize import anneal
from scipy.optimize import fmin_tnc
from CurveFitDEs import eq2 as Sys
from CurveFitFuncs import SumofSquares as SS
from CurveFitFuncs import Resids as R
import matplotlib.pyplot as plt
import csv
from sortinghat import sorting_hat as st
from scipy.optimize import fminbound

def Fval(par):
    a=par[0]
    b=par[1]
    return a*b

answer=fmin_tnc(Fval,[0.1,0.15],bounds=[(.10,.20),(.10,.20)],approx_grad=True)

a=fmin_tnc(Fval,[0.5,0.05],bounds=[(0.1,3.0),(0.1,1.0)],approx_grad=True)
